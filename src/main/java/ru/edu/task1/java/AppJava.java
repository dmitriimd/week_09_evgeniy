package ru.edu.task1.java;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Класс для настройки контекста контейнера зависимостей.
 */
@Configuration
public class AppJava {

    @Bean(initMethod="init")
    public ComponentC c() {
        return new ComponentC();
    }

    @Bean
    public ComponentB b() {
        return new ComponentB("stringForComponentB");
    }

    @Bean
    public ComponentA a() {
        return new ComponentA(c());
    }

    @Bean
    public MainContainer mainContainer() {
        return new MainContainer();
    }


    public static MainContainer run() {
        ApplicationContext context = new AnnotationConfigApplicationContext(AppJava.class);
        return context.getBean(MainContainer.class);
    }

    public static void main(String[] args) {
        run().isValid();
    }
}
