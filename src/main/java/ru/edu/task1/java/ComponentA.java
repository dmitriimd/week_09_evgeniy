package ru.edu.task1.java;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

/**
 * ReadOnly
 */
@ComponentScan("ru.edu.task1.java")
@Component
public class ComponentA {

    private ComponentC componentC;

    public ComponentA(ComponentC c) {
        componentC = c;
    }

    public boolean isValid(){
        return componentC != null && componentC.isValid();
    }
}
