package ru.edu.task1.java;

import org.springframework.stereotype.Component;

/**
 * ReadOnly. Можно только добавлять аннотации.
 */
@Component
public class ComponentB {

    private String string;

    public ComponentB(String string) {
        this.string = string;
    }

    public boolean isValid() {
        return "stringForComponentB".equals(string);
    }
}
