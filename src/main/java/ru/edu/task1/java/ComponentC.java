package ru.edu.task1.java;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 * ReadOnly. Можно только добавлять аннотации.
 */
@Component
public class ComponentC {

    private boolean isInit;

    @Bean
    public void init() {
        isInit = true;
    }


    public boolean isValid() {
        return isInit;
    }
}
