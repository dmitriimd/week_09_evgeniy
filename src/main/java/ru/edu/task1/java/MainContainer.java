package ru.edu.task1.java;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

/**
 * ReadOnly. Можно только добавлять аннотации.
 */
@ComponentScan("ru.edu.task1.java")
public class MainContainer {

    @Autowired
    private ComponentA componentA;

    @Autowired
    private ComponentB componentB;

    @Bean
    public boolean isValid() {
        System.out.println("componentA = " + (componentA));
        System.out.println("componentB = " + (componentB));
        System.out.println("componentA is Valid?: " + componentA.isValid());
        System.out.println("componentB is Valid?: " + componentB.isValid());
        return componentA != null && componentB != null && componentA.isValid() && componentB.isValid();
    }
}
