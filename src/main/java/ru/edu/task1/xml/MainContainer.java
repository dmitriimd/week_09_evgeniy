package ru.edu.task1.xml;

/**
 * ReadOnly
 */
public class MainContainer {

    private ComponentA componentA;
    private ComponentB componentB;

    public MainContainer(ComponentA a, ComponentB b) {
        componentA = a;
        componentB = b;
    }

    public boolean isValid() {
        System.out.println("componentA = " + (componentA));
        System.out.println("componentB = " + (componentB));
        System.out.println("componentA is Valid?: " + componentA.isValid());
        System.out.println("componentB is Valid?: " + componentB.isValid());
        return componentA != null && componentB != null && componentA.isValid() && componentB.isValid();
    }
}
