package ru.edu.task2.xml;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * ReadOnly
 */
public class AppXML {
    public static MainContainer run() throws InterruptedException {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("task_02.xml");
        return context.getBean(MainContainer.class);
    }
}
