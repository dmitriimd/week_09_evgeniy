package ru.edu.task2.xml;

/**
 * ReadOnly
 */
public class MainContainer {

    private TimeKeeper keeper;

    private Child child;

    public MainContainer(TimeKeeper keeperBean, Child childBean) throws InterruptedException {
        keeper = keeperBean;
        child = childBean;
    }

    public boolean isValid() {
        System.out.println(keeper);
        System.out.println(child);
        System.out.println(child.getTimeKeeper());

        if(keeper == null || child == null){
            throw new RuntimeException("Есть пустая зависимость");
        }
        if(keeper.equals(child.getTimeKeeper())){
            throw new RuntimeException("Зависимости не должны совпадать");
        }
        return true;
    }
}
