package ru.edu.task3.java;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Класс для настройки контекста контейнера зависимостей.
 */
@Configuration
@ComponentScan
public class AppJava {

//    @Bean
//    public DependencyObject debugDependency() {
//        return new DebugDependency();
//    }
//    @Bean
//    public DependencyObject promDependency() {
//        return new PromDependency();
//    }
//  В этом случае непонятно как передать в конструктор экземпляр
//    @Bean
//    public MainContainer mainContainer() {
//        return new MainContainer();
//    }

    public static MainContainer run(String profile){
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        context.getEnvironment().setActiveProfiles(profile);
        context.register(AppJava.class);
        context.refresh();
        return context.getBean(MainContainer.class);
    }
}
