package ru.edu.task3.xml;

/**
 * ReadOnly
 */
public interface DependencyObject {
    String getValue();
}
