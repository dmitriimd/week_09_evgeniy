package ru.edu.task3.xml;

import org.springframework.beans.factory.annotation.Qualifier;

/**
 * ReadOnly
 */
public class MainContainer {

    private DependencyObject dependency;

    public MainContainer(DependencyObject dependency) {
        this.dependency = dependency;
    }

    public String getValue() {
        return dependency.getValue();
    }
}
