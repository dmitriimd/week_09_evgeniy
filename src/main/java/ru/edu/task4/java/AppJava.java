package ru.edu.task4.java;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Класс для настройки контекста контейнера зависимостей.
 */
@Configuration
public class AppJava {

    @Bean
    public RealService realService() {
        return new RealService();
    }

    @Bean
    public CacheService cacheService() {
        return new CacheService(realService());
    }

    @Bean
    public MainContainer mainContainer () {
        return new MainContainer(cacheService());
    }

    public static MainContainer run(){
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AppJava.class);
        return context.getBean(MainContainer.class);
    }
}
