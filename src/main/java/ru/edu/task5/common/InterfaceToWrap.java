package ru.edu.task5.common;

/**
 * ReadOnly
 */
public interface InterfaceToWrap {
    String getValue();
}
