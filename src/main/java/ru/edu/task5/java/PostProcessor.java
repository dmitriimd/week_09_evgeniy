package ru.edu.task5.java;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.stereotype.Component;
import ru.edu.task5.common.InterfaceToRemove;
import ru.edu.task5.common.InterfaceToWrap;

public class PostProcessor implements BeanPostProcessor {

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        Wrapper wrappedBaen = null;
        if(bean instanceof InterfaceToWrap) {
            wrappedBaen = new Wrapper(bean);
        }
        if(bean instanceof InterfaceToRemove) {
            return "";
        }
        return wrappedBaen;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        return BeanPostProcessor.super.postProcessAfterInitialization(bean, beanName);
    }

}

class Wrapper implements InterfaceToWrap {
    private Object bean;
    private String name;
    public Wrapper (Object bean) {
        this.bean = bean;
        name = "wrapped " + ((InterfaceToWrap)bean).getValue();
    }
    public String getValue() {
        return name;
    }
}
