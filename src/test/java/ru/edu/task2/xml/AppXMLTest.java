package ru.edu.task2.xml;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * ReadOnly
 */
public class AppXMLTest {

    @Test
    public void run() throws InterruptedException {
        assertTrue(AppXML.run().isValid());
    }
}